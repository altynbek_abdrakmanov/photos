ActiveAdmin.register Comment do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  index do
    selectable_column
    id_column

    column :note do |product|
      link_to product.title, admin_product_path(product)
    end
    column :user
    column :rate
    column :picture
    actions
  end

end
