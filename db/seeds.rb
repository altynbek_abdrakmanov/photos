# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures')

admin = User.create(name: 'Admin', email: 'admin@example.com', password: '123456789', password_confirmation: '123456789', admin: true)

user = User.create(name: 'user', email: 'asa@example.com', password: '123456789', password_confirmation: '123456789', admin: false)

arzu = Picture.create(title: 'Arzu', image: File.new(fixtures_path.join('arzu.png')), user: user)
supara = Picture.create(title: 'Arzu', image: File.new(fixtures_path.join('supara.png')), user: admin)
diar = Picture.create(title: 'Arzu', image: File.new(fixtures_path.join('diar.png')), user: user)

Comment.create(note: Faker::Lorem.paragraph, user: admin, picture: arzu, rate: 5)
Comment.create(note: Faker::Lorem.paragraph, user: user, picture: supara, rate: 5)
Comment.create(note: Faker::Lorem.paragraph, user: admin, picture: diar, rate: 5)