class PicturesController < ApplicationController

  def index
    @pictures = Picture.order(created_at: :desc)
  end

  def new
    @picture = Picture.new
  end

  def create
    @picture = current_user.pictures.build(picture_params)

    if @picture.save
      redirect_to @picture
    else
      render 'new'
    end
  end

  def edit
    @picture = Picture.find(params[:id])
  end

  def update
    @picture = Picture.find(params[:id])

    if @picture.update(picture_params)
      redirect_to @picture
    else
      render 'edit'
    end
  end

  def show
    @picture = Picture.find(params[:id])
    @comment = Comment.new
    @comments = Comment.all
  end

  def create_comment
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      redirect_to root_path
    end
  end

  def destroy
    Picture.destroy(params[:id])

    redirect_to user_path
  end

  private

  def picture_params
    params.require(:picture).permit(:name, :user_id, :image)
  end

  def comment_params
    params.require(:comment).permit(:note, :rate, :user_id, :picture_id)
  end

end
